
@extends('layouts.app')

@section('content')
<div class="chiller-theme toggled bg-primary" style="background: linear-gradient(to bottom, #000066 0%, #009999 100%);" >
	@include('sidebar.sidebar')
	 <main class="">
	    <div class="container-fluid">
	    	<div class="row justify-content-center">
		      	<div class="card col-md-4 offset-4 m-5">
						<div class="card-header m-auto">
							<img src="{{ asset($product->img_path) }}" style="height: 100px;" class="img-fluid">
						</div>

						<div class="card-body">
								<div class="form-group">
									<label for="name">Name:</label>
									<input class="form-control" type="text" id="name" name="name" autocomplete="off" value="{{ $product->name }}" disabled>
									@if($errors->has('name'))
									   <div class="error" style="color: red;" >{{ $errors->first('name') }}</div>
									@endif
								</div>
								<div class="form-group" >
									<label for="description">Description:</label>
									<textarea class="form-control text-center" type="text" id="description" name="description" autocomplete="off" value="{{ $product->description }}" style="height: 150px;" disabled> {{ $product->description }}</textarea>
									@if($errors->has('description'))
									    <div class="error" style="color: red;" >{{ $errors->first('description') }}</div>
									@endif
								</div>
								<!-- category select dropdown -->
								<div class="form-group">
									<label for="category_id">Category:</label>
									<select class="form-control" id="category_id" name="category" disabled>
									
										@if(count($categories)>0)
											@foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->name }}</option>
											@endforeach
										@endif
									</select>
								</div>


								
						</div>
				</div>
			</div>

		</div>
	</main>
</div>
@endsection