
@extends('layouts.app')

@section('content')
<div class="chiller-theme">
@include('sidebar.sidebar')
 <main class="">
    <div class="container-fluid">
           <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800 text-center">Edit Assets</h1>
          <div class="row">
          	<div class="col-8 offset-2">
          		<div id="accordion">
          			<!-- card for add category form -->
          			<div class="card">
          				<div class="card-header">
          					<a class="card-link h6" href="#addCategory">
          						Add Category
          					</a>
          				</div>

          				<div id="addCategory" class="" data-parent="#accordion">
          					<div class="card-body">
          						<form method="post" action="/categories">
                                        @csrf
          							<div class="form-group">
          								<label for="name">Category name: </label>

          								<input class="form-control" type="text" id="catName" name="category">
                                    @if($errors->has('category'))
                                            <div class="error" style="color: red;" >{{ $errors->first('category') }}</div>
                                         @endif
                                    <label for="name">Asset Code: </label>
                                    <input class="form-control" type="text" id="catName" name="assetcode">
                                    @if($errors->has('assetcode'))
                                            <div class="error" style="color: red;" >{{ $errors->first('assetcode') }}</div>
                                         @endif
                                         <div class="text-center mt-3"> 
                                    <button type='submit' class="btn btn-outline-info"  id="addCatBtn">Add Category</button>
                                  </div>
          							</div>
          						</form>
          						
          					</div>	
          				</div>
          			</div>
          			<!-- end of add category card -->

          			<!-- add product card -->
          			<div class="card">
          				<div class="card">
          					<div class="card-header">
          						<a class="card-link h6" data-toggle="collapse" href="#addProduct">
          							Add Product
          						</a>
          					</div>

          					<div id="addProduct" class="" data-parent="#accordion">
          						<div class="card-body">
          							<form method="post" action="/products" enctype="multipart/form-data">
          								@csrf
          								<div class="form-group">
          									<label for="name">Name:</label>
          									<input class="form-control" type="text" id="name" name="name" autocomplete="off">
          									@if($errors->has('name'))
          									   <div class="error" style="color: red;" >{{ $errors->first('name') }}</div>
          									@endif
          								</div>
          								<div class="form-group">
          									<label for="description">Description:</label>
          									<input class="form-control" type="text" id="description" name="description" autocomplete="off">
          									@if($errors->has('description'))
          									    <div class="error" style="color: red;" >{{ $errors->first('description') }}</div>
          									@endif
          								</div>
          								<div class="form-group">
                            <label for="image">Image:</label>
                               <input class="form-control" type="file" id="image" name="image">
                               @if($errors->has('image'))
                                   <div class="error" style="color: red;" >{{ $errors->first('image') }}</div>
                               @endif
                          </div>
          								<!-- category select dropdown -->
          								<div class="form-group">
          									<label for="category_id">Category:</label>
          									<select class="form-control" id="category_id" name="category">
          										<option>Select a category:</option>
          										@if(count($categories)>0)
          											@foreach($categories as $category)
          												<option value="{{ $category->id }}">{{ $category->name }}</option>
          											@endforeach
          										@endif
          									</select>
          								</div>
          								
          								<div class="text-center">
          								  <button type="submit"class="btn btn-outline-info">Add Product</button>
                          </div>
          							</form>
          						</div>
          					</div>
          				</div>
          			</div>
          			<!-- end of add product card -->
          		</div>
          		<!-- end of accordion -->
          	</div>
          </div>

          </div>
        </main>
      </div>

   

@endsection