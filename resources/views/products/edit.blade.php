
@extends('layouts.app')

@section('content')
<div class="chiller-theme toggled">
	@include('sidebar.sidebar')
	 <main class="">
	    <div class="container-fluid">
	    	<div class="row">
		      	<div class="card col-md-4 offset-4">
						<div class="card-header m-auto border border-info">
							<img src="{{ asset($product->img_path) }}" style="height: 100px;" class="img-fluid ">
						</div>

						<div class="card-body">
							<form method="POST" action="/products/{{$product->id}}" enctype="multipart/form-data">
								@csrf
								@method('PUT')
								<div class="form-group">
									<label for="name">Name:</label>
									<input class="form-control" type="text" id="name" name="name" autocomplete="off" value="{{ $product->name }}">
									@if($errors->has('name'))
									   <div class="error" style="color: red;" >{{ $errors->first('name') }}</div>
									@endif
								</div>
								<div class="form-group" >
									<label for="description">Description:</label>
									<textarea class="form-control text-center" type="text" id="description" name="description" autocomplete="off" value="{{ $product->description }}" style="height: 150px;"> {{ $product->description }}</textarea>
									@if($errors->has('description'))
									    <div class="error" style="color: red;" >{{ $errors->first('description') }}</div>
									@endif
								</div>
								<div class="form-group">
		          					<label for="image">Image:</label>
		             				<input class="form-control" type="file" id="image" name="image" value="{{ asset($product->img_path) }}">
		             				@if($errors->has('image'))
		                 			<div class="error" style="color: red;" >{{ $errors->first('image') }}</div>
		             				@endif
		        				</div>
								<!-- category select dropdown -->
								<div class="form-group">
									<label for="category_id">Category:</label>
									<select class="form-control" id="category_id" name="category">
									
										@if(count($categories)>0)
											@foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->name }}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="text-center">
									<button type="submit"class="btn btn-success">Edit Product</button>
								</div>


								
							</form>
						</div>
				</div>
			</div>

		</div>
	</main>
</div>
@endsection