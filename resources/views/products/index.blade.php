
@extends('layouts.app')

@section('content')
<div class="chiller-theme toggled">
@include('sidebar.sidebar')
 <main class="">
    <div class="container-fluid">
      @can('auditor')
      <div class="well well-sm">
          <div class="row">
            @foreach($products as $product)
            <div class="item  col-xs-4 col-lg-3 mb-2">
                <div class="thumbnailone shadow">
                  
                  <img class="group list-group-image mx-auto mt-2" src="{{ asset($product->img_path) }}" alt="" style="width: 50%; height:100px; display: block; " />
                
                    <div class="caption m-2">
                        <h5 class="group inner list-group-item-heading mb-2" style="height: 55px; font-size: 20px">
                           <a href="/products/{{$product->id}}">{{$product->name}}</a>
                          </h5>
                          <small class="mt-2">Created at: {{ $product->created_at }}</small>
                          <p class="group inner list-group-item-text" style="height: 80px; font-size: 10px;">
                              {{ $product->description }}</p>
                          
                          <div class="row">
                            @if($product->isActive == 1)
                            <div class="col-xs-12 col-md-12">
                              <form method="POST" action="/assets" class="col mb-2">
                                  @csrf
                                  <div class="text-center">
                                    <input type="text" name="prodid" value="{{ $product->id }}" hidden>
                                    <input type="number" name="quantity" min="1" placeholder="Input Quantity" class="form-group">
                                    <button class="btn btn-dark btn-sm" type="submit">Submit</button>
                                  </div>
                              </form> 
                                
                            </div>
                            @endif
                          </div>
                          
                          <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <a href="/products/{{$product->id}}/edit" class="btn btn-outline-success btn-block"> Edit</a>
                            </div>
                              
                            <form method="POST" action="/products/{{$product->id}}" class="col-xs-12 col-md-6">
                                @csrf
                                @method('DELETE')
                                {{-- check if product is active --}}
                                @if($product->isActive == 1)
                                  <button type="submit" class="col btn btn-outline-danger">Deactivate</button>
                                @else
                                  <button type="submit" class="col btn btn-outline-danger">Reactivate</button>
                                  @endif 
                            </form>
                                 
                          </div>
                    </div>
                  </div>
            </div>
            @endforeach
          </div>
          {{-- end row for each product --}}
      </div>
{{-- end of product viiew --}}
@else
{{-- non admin catalogue view --}}
@if (count($errors) > 0)
        <div class="alert alert-danger bg-light">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
@endif



      <div class="well well-sm">
        <div class="row">
        @foreach($assets as $asset)
          <div class="item  col-xs-4 col-lg-3">
            <div class="thumbnailone mb-3 shadow">
              <img class="group list-group-image mx-auto mt-2" src="{{ asset($asset->product->img_path) }}" alt="" style="width: 50%; height:100px; display: block; " />
              
                <div class="caption m-2">
                    <h4 class="group inner list-group-item-heading text-center" style="height: 20px; font-size: 15px">
                       <a href="/products/{{$asset->product->id}}">{{$asset->product->name}}</a>
                     </h4>
                     <h4 class="group inner list-group-item-heading text-center" style="height: 17px; font-size: 15px"> {{ $asset->product->category->name }}</h4>

                       <small class="mt-2">Serial ID: {{ $asset->serialNo }}</small>
                    <p class="group inner list-group-item-text" style="height: 60px; font-size: 10px;">
                        {{ $asset->product->description }}</p>

                        <div class="row">
                          <form method="POST" action="/transactions" class="text-center m-auto">
                          @csrf
                          @if($asset->isAvailable == 1)
                          
                            <div class="col-12 mt-1">
                              <p>Request Now!</p>
                            </div>
                            <input type="" name="prodid" value="{{ $asset->product->id }}" hidden>
                            <input type="" name="prodserial" value="{{ $asset->serialNo }}" hidden>
                            <div class="col-12">
                               <label for="borrowed">Borrow Date:</label>
                                <input type="date" name="borrowed">
                                
                            </div>
                            <div class="col-12 mb-2">
                                <label for="teturned">Return Date:</label>
                                <input type="date" name="returned">
                              
                            </div>
                            @endif

                            <div class="col-12 m-auto">
                              @if($asset->isAvailable == 1)
                                <button type="submit" class="col btn btn-info text-white" style="background-color: #4f6d7a;">Request Asset</button>
                            @else
                                <button type="submit" class="btn btn-info text-white mt-5" style="background-color: #4f6d7a;" disabled>Unavailable</button>
                                @endif
                              
                            </div>
                            </form>

                        </div>
                   
                </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>


    </div>

  </main>
              
</div>
@endcan

@endsection
