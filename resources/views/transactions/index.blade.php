

@extends('layouts.app')

@section('content')
<div class=" chiller-theme">
@include('sidebar.sidebar')
 <main class="">
    <div class="">

      	{{-- auditor page --}}
@can('auditor')
		<h4 class="text-center mb-5">MANAGE BOOKINGS</h4>
		<div class="row">
			<div class="col-md-12">
			
				<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">

					<thead>
						<h4 class="text-center"  style="text-transform: uppercase;">Pending Request Table</h4>
						<tr>
							<th class="th-sm">Name</th>
							<th class="th-sm">Department</th>
							<th class="th-sm">Category</th>
							<th class="th-sm">Product Name</th>
							<th class="th-sm">Borrow Date</th>
							<th class="th-sm">Return Date</th>
							<th class="th-sm">Request Date</th>
							<th class="th-sm">Action</th>
						</tr>
					</thead>
					
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 1)
						<tr>
							<td>{{ $transaction->user->empFirstName }} <span>{{ $transaction->user->empLastName }}</span></td>
							<td>{{ $transaction->user->department_code}}</td>
							<td>{{ $transaction->product->category->name}}</td>
							<td><a href="/products/{{$transaction->product->id  }}">{{ $transaction->product->name }}</a></td>
							<td>{{ $transaction->borrowDate }}</td>
							<td>{{ $transaction->returnedDate }}</td>
							<td>{{ $transaction->created_at }}</td>
							<td>
							<form method="POST" action="/transactions/{{$transaction->id}}">
								@csrf
								@method('PUT')
								<input type="submit" class="btn btn-sm" value="Approved" name="approved" style="background-color: #87bba2;" onclick="return confirm('Do you really want to Approve this Booking?')">
								<input type="submit" class="btn btn-sm btn-primary" value="Rejected" name="rejected" onclick="return confirm('Do you really want to Reject this Booking?')">
							</form>
							</td>
						</tr>
						@endif
						@endforeach
					</tbody>
					
				</table>
			</div>
		</div>
		{{-- 2nd table --}}
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
					<thead>
						<h4 class="mt-5 text-center" style="text-transform: uppercase;">Approved Request Table</h4>
						<tr>
							<th class="th-sm">Reference</th>
							<th class="th-sm">Name</th>
							<th class="th-sm">Department</th>
							<th class="th-sm">Category</th>
							<th class="th-sm">Asset_id</th>
							<th class="th-sm">Borrow Date</th>
							<th class="th-sm">Return Date</th>
							<th class="th-sm">Request Date</th>
							<th class="th-sm">Action</th>
						</tr>
					</thead>
					
					<tbody>
						@foreach($transactions as $transaction)
							@if($transaction->status_id == 4)
						<tr>
							<td>{{ $transaction->referenceNo }}</td>
							<td>{{ $transaction->user->empFirstName }} <span>{{ $transaction->user->empLastName }}</span></td>
							<td>{{ $transaction->user->department_code}}</td>
							<td>{{ $transaction->product->category->name}}</td>
							<td><a href="/products/{{ $transaction->product_id }}" >{{ $transaction->asset_serial}}</a></td>
							<td>{{ $transaction->borrowDate }}</td>
							<td>{{ $transaction->returnedDate }}</td>
							<td>{{ $transaction->updated_at }}</td>
							<td>
								<form method="POST" action="/transactions/{{$transaction->id}}">
								@csrf
								@method('PUT')
								<input type="submit" class="btn btn-warning btn-sm" value="returned" name="returned" hidden>
								<button class="btn btn-info btn-sm" type="submit"> Mark as Returned</button>
							</form>
								
							</td>
						</tr>
						@endif
						@endforeach
					</tbody>
					
				</table>
			</div>
		</div>
			{{-- 3rd table --}}
		<div class="row" >
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-sm border" cellspacing="0" width="100%">

					<thead>
						<h4 class="mt-5 text-center"  style="text-transform: uppercase;">Completed Request Table</h4>
						<tr>
							<th class="th-sm">Reference No</th>
							<th class="th-sm">Name</th>
							<th class="th-sm">Asset ID</th>
							<th class="th-sm">Borrow Date</th>
							<th class="th-sm">Return Date</th>
							<th class="th-sm">Status</th>
							<th class="th-sm">Completion Date</th>

						</tr>
					</thead>
					
					<tbody>
						<tr>
							@foreach($transactions as $transaction)
							@if($transaction->status_id == 3 OR $transaction->status_id == 5)
							<td>{{ $transaction->referenceNo }}</td>
							<td>{{ $transaction->user->empFirstName }} <span>{{ $transaction->user->empLastName }}</span></td>

							@if($transaction->status_id == 2 OR $transaction->status_id == 3)
							<td>N/A</td>
							@else
							<td>{{ $transaction->asset_serial }}</td>
							@endif
							<td>{{ $transaction->borrowDate }}</td>
							<td>{{ $transaction->updated_at }}</td>
							<td>{{ $transaction->status->name }}</td>
							<td>{{ $transaction->updated_at }}</td>
						</tr>
						@endif
						@endforeach
						
					</tbody>
				</table>
			</div>
		</div>



@else
{{-- table for non admin --}}
		<div class="row">
			<div class="col-md-12">
			
				<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">

					<thead >
						<h4 class="mt-5 text-center"  style="text-transform: uppercase;">Pending Request Table</h4>
						<tr>
							<th class="th-sm">Request id</th>
							<th class="th-sm">Product Line</th>
							<th class="th-sm">Borrow Date</th>
							<th class="th-sm">Return Date</th>
							<th class="th-sm">Request Date</th>
							<th class="th-sm">Action</th>
						</tr>
					</thead>
					
					<tbody>
						@foreach($transactions as $transaction)
							@if(Auth::user()->id == $transaction->user_id)
							@if($transaction->status_id == 1)
						<tr>
							<td class="text-center">{{ $loop->iteration }}</td>
							<td><a href="/products/{{$transaction->product->id  }}">{{ $transaction->product->name }}</a></td>
							<td>{{ $transaction->borrowDate }}</td>
							<td>{{ $transaction->returnedDate }}</td>
							<td>{{ $transaction->created_at }}</td>
							<td>
							<form method="POST" action="/transactions/{{$transaction->id}}" class="text-center">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn btn-outline-danger btn-sm" onclick="return confirm('Do you really want to Cancel this Booking')"> Cancel </button>
							</form>
							</td>
						</tr>
						@endif
						@endif
						@endforeach
					</tbody>
					
				</table>
			</div>
		</div>
		{{-- 2nd table --}}
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
					<thead>
						<h4 class="mt-5 text-center"  style="text-transform: uppercase;">Approved Request Table</h4>
						<tr>
							<th class="th-sm">Reference No.</th>
							<th class="th-sm">Asset ID</th>
							<th class="th-sm">Borrow Date</th>
							<th class="th-sm">Return Date</th>
							<th class="th-sm">Approval Date</th>
						</tr>
					</thead>
					
					<tbody>
						<tr>
							@foreach($transactions as $transaction)
							@if(Auth::user()->id == $transaction->user_id AND $transaction->status_id == 4)
							<td>{{ $transaction->referenceNo }}</td>
							<td><a href="/products/{{ $transaction->product_id }}" >{{ $transaction->asset_serial}}</a></td>
							<td class="text-center">{{ $transaction->borrowDate }}</td>
							<td class="text-center">{{ $transaction->returnedDate }}</td>
							<td class="text-center">{{ $transaction->created_at }}</td>
							
						</tr>
							@endif
							@endforeach
					</tbody>
					
				</table>
			</div>
		</div>
			{{-- 3rd table --}}
		<div class="row" >
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">

					<thead>
						<h4 class="mt-5 text-center"  style="text-transform: uppercase;">Completed Request Table</h4>
						<tr>
							<th class="th-sm">Reference No.</th>
							<th class="th-sm">Asset ID</th>
							<th class="th-sm">Borrow Date</th>
							<th class="th-sm">Return Date</th>
							<th class="th-sm">Status</th>
							<th class="th-sm">Completion Date</th>

						</tr>
					</thead>
					
					<tbody>
						<tr>
							@foreach($transactions as $transaction)
							@if(Auth::user()->id == $transaction->user_id)
							@if($transaction->status_id == 2 OR $transaction->status_id == 3 OR $transaction->status_id == 5)
							<td>{{ $transaction->referenceNo }}</td>
							@if($transaction->status_id == 2 OR $transaction->status_id == 3)
							<td>N/A</td>
							@else
							<td>{{ $transaction->asset_serial }}</td>
							@endif
							<td>{{ $transaction->borrowDate }}</td>
							<td>{{ $transaction->updated_at }}</td>
							<td>{{ $transaction->status->name }}</td>
							<td>{{ $transaction->updated_at }}</td>
						</tr>
						@endif
						@endif
						@endforeach
						
					</tbody>
				</table>
			</div>
		</div>


@endcan




      </div>
  </main>
</div>
 	
@endsection