<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
          <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: black;
                font-family: 'Nunito', sans-serif;
                font-weight: 600;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height" style="background:url({{ asset('images/background.jpg') }});background-size: cover;opacity: 0.75; ">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="text-white">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="text-white">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="text-white">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="page-header" >
                <div class="container">
                    <div class="row">
                        <div class="jumbotron text-center" style="background: white; height: 400px;">
                           <h1 class="h-40 font-weight-bold" >BATCH50 CSP2</h1>
                           <h2>Credentials:</h2>
                           <h4 class="font-weight-bold">ADMIN(Audit Department)</h4>
                           <h6>Username: audit@gmail.com</h6>
                           <h6>Password: Test12345</h6>
                           <h4 class="font-weight-bold">NON-ADMIN(HR Department)</h4>
                           <h6>Username: hrd@gmail.com</h6>
                           <h6>Password: Test12345</h6>

                            
                        </div>
                        
                    </div>
                    
                </div>
                

            </div>
           


        </div>
    </body>
</html>
