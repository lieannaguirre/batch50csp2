 
  @php($profiles = App\Profile::all())
  @php($departments = App\Department::all())
  @foreach($profiles as $profile)
  @if($profile->user_id == Auth::user()->id)
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper d-none d-lg-block">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="#">BATCH50CSP2</a>
        <div id="close-sidebar" hidden>
          <i class="fas fa-times"></i>
        </div>
      </div>
      
     <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="{{ asset(Auth::user()->img_path) }}"
            alt="User picture">
        </div>
        <div class="user-info" style="color: #aed9e0;">
          <span class="user-name"><strong>{{Auth::user()->empFirstName. " ".
           Auth::user()->empLastName }}</strong></span>
          @foreach($departments as $department)
          @if(Auth::user()->department_code == $department->code)
          <span class="user-role">{{$department->deptName}}</span>
          @endif
          @endforeach 
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>
      
      <!-- sidebar-header  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>General</span>
          </li>
          <li class="">
            <a href="/dashboard">
              <i class="fa fa-tachometer-alt"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li class="">
            <a href="/profiles">
              <i class="fa fa-tachometer-alt"></i>
              <span>View my Profile</span>
            </a>
          </li>
          <li class="">
            <a href="/profiles/{{ $profile->id }}/edit">
              <i class="fa fa-tachometer-alt"></i>
              <span>Edit my Profile</span>
            </a>
          </li>
           <li class="">
            <a href="/products">
              <i class="fa fa-tachometer-alt"></i>
              <span>Available assets</span>
            </a>
          </li>
          <li class="">
            <a href="/transactions">
              <i class="fa fa-folder"></i>
              <span>Assets Dashboard</span>
            </a>
          </li>
      </div>
    </div>
  </nav>
  <!-- sidebar-wrapper  -->

@endif
@endforeach

{{-- 




 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar" style="width: 10%;">
  @php($profiles = App\Profile::all())
  @foreach($profiles as $profile)
  @if($profile->user_id == Auth::user()->id)
  
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="myprofile.php">
       
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Batch50CSP2</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>
    

      <!-- Nav Item - Utilities Collapse Menu -->
     
   
      Nav Item - Charts
      <li class="nav-item">
        <a class="nav-link" href="/profiles/{{ $profile->id }}">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
    
       <li class="nav-item">
        <a class="nav-link" href="/profiles/{{ $profile->id }}/edit">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Edit my Profile</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/products">
          <i class="fas fa-fw fa-table"></i>
          <span>View Available Products</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/transactions">
          <i class="fas fa-fw fa-table"></i>
          <span>Assets Dashboard</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
      @endif
      @endforeach
    </ul>
     --}}