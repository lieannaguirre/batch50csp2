@extends('layouts.app')

@section('content')

<div id="chiller-theme toggled">
@include('sidebar.sidebar')
  <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
    <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <div class="row">
          <div class="wrapperbody">
            <div class="containerid">
              <img src="{{ asset( Auth::user()->img_path) }}" alt="" class="profile-img">
              
              <div class="contentid">
                <div class="sub-content">
                  <h1>{{ Auth::user()->empFirstName . ' ' . Auth::user()->empLastName }}</h1>
                  <span>{{ Auth::user()->email }}</span>
                  @foreach($departments as $department)
                  @if(Auth::user()->department_code ==  $department->code)
                    <p style="font-size: 22px;">{{ $department->deptName}}</p>
                  @endif
                  @endforeach
                  <p class="location"><i class="fa fa-map-marker" aria-hidden="true"></i></p>
                </div>
              </div>
            </div>
            <a href="/profiles/create" class="btn btn-outline-info btn-block btn-lg" style="margin-top: 20px;">Create Profile</a>
          </div>
        </div>
      




        

           

          

      </div>
    </div>
  </div>

</div>



@endsection