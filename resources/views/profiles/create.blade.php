<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@300&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style2.css') }}" rel="stylesheet">

    {{-- fontawesomelink --}}
    <script src="https://kit.fontawesome.com/502d03693a.js" crossorigin="anonymous"></script>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <div id="app" class="">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm m-2" style="height: 70px;">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('images/logo.jpg') }}" width="170" height="50" alt="">
                  </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><img src="{{ asset(Auth::user()->img_path) }}" style="height: 25px;" class="m-2">
                                    {{ Auth::user()->empFirstName." ".Auth::user()->empLastName }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="container-fluid">
<div class="page-wrapper chiller-theme toggled">

 <main class="page-content">
    <div class="container-fluid">
        <div class="row">
          <div class="wrapperbody">
            <div class="containerid">
              <img src="{{ asset( Auth::user()->img_path) }}" alt="" class="profile-img">
              <div class="contentid">
                <div class="sub-content">
                  <h1>{{ Auth::user()->empFirstName . ' ' . Auth::user()->empLastName }}</h1>
                  <span>{{ Auth::user()->email }}</span>
                  @foreach($departments as $department)
                  @if(Auth::user()->department_code ==  $department->code)
                  <p style="font-size: 22px;">{{ $department->deptName}}</p>
                  @endif
                  @endforeach
                  <h4 class="font-italic" style="color: red;">"Please create your profile"</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
          <form class="form-group row" method="Post" action="/profiles">
            @csrf
            <div class="col-lg-3 offset-4 mt-5" >
              <label for="FirstName" class="">First Name: </label>
               <input type="text" class="form-control form-control-user" name="firstname" value="{{  Auth::user()->empFirstName }}" readonly>
            </div>

            <div class="col-lg-3  mt-5" >
              <label for="lastname" class="">Last Name: </label>
               <input type="text" class="form-control form-control-user"  name="lastname" value="{{  Auth::user()->empLastName }}" readonly>
            </div>

             <div class="col-lg-6 offset-4 mt-5" >
              <label for="designation" class="">Employee Designation: </label>
               <input type="text" class="form-control form-control-user" name="designation">
               @if($errors->has('designation'))
                     <div class="error" style="color: red;" >{{ $errors->first('designation') }}</div>
                 @endif
            </div>
             

            <div class="col-lg-6 offset-4 mt-5" >
              <label for="city" class="">City: </label>
               <input type="text" class="form-control form-control-user" id="City" name="city" required="true">
               @if($errors->has('city'))
              <div class="error" style="color: red;" >{{ $errors->first('city') }}</div>
            @endif
            </div>
            


            <div class="col-lg-6 offset-4 mt-5" >
              <label for="contact" class="">Employee Contact: </label>
               <input type="number" class="form-control form-control-user" id="contact" name="contact" aria-describedby="emailHelp">
               @if($errors->has('contact'))
                   <div class="error" style="color: red;" >{{ $errors->first('contact') }}</div>
              @endif
            </div>
            

            <div class="col-lg-3 offset-4 mt-5">
              <label for="Gender" class="">Employee Gender: </label>
              <select class="form-control form-control-user" id="gender" name="gender" >
                <option value="Male" selected>Male</option>
                <option value="Female">Female</option>
              </select>
            </div>


            <div class="col-lg-3 mt-5">
              <label for="hiredate" class="">Hire Date: </label>
               <input type="date" class="form-control form-control-user" id="hiredae" name="hiredate" aria-describedby="emailHelp" required>
                @if($errors->has('hiredate'))
                   <div class="error" style="color: red;" >{{ $errors->first('hiredate') }}</div>
               @endif
            </div>
            

            <div class="col-lg-6 offset-4 mt-5">
              <label for="salary" class="">Salary: </label>
               <input type="number" step="0.01" class="form-control form-control-user" id="salary" name="salary" aria-describedby="emailHelp" >
               @if($errors->has('salary'))
                   <div class="error" style="color: red;" >{{ $errors->first('salary') }}</div>
               @endif
            </div>
            
           

            <div class="text-center col-lg-6 offset-4 mt-5">
            <button type="submit" class="btn btn-primary btn-lg">Submit</button>
            </div>
          </form>



        

           

          
</div>
</main>
</div>
