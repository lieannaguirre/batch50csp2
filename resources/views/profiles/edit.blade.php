
@extends('layouts.app')

@section('content')
<div class=" chiller-theme toggled">
@include('sidebar.sidebar')
 <main class="">
    <div class="container-fluid">
        <div class="row">
          <div class="wrapperbody">
            <div class="containerid">
              <img src="{{ asset( Auth::user()->img_path) }}" alt="" class="profile-img">
              <div class="contentid">
                <div class="sub-content">
                  <h1>{{ Auth::user()->empFirstName . ' ' . Auth::user()->empLastName }}</h1>
                  <span>{{ Auth::user()->email }}</span>
                  @foreach($departments as $department)
                  @if(Auth::user()->department_code ==  $department->code)
                  <p style="font-size: 22px;">{{ $department->deptName}}</p>
                  @endif
                  @endforeach
                  <p class="location"><i class="fa fa-map-marker" aria-hidden="true"></i>{{ $profile->city }}</p>
                  <p class="location" style="font-size: 24px"><i class="fa fa-id-card-o mr-1" aria-hidden="true" ></i>{{ $profile->employeenumber }}</p>
                </div>
              </div>
            </div>
        
            <a href="/profiles/{{ $profile->id }}/edit" class="btn btn-outline-info btn-block btn-lg" style="margin-top: 20px;">Edit Profile</a>  
          </div>
        </div>
        <div class="row">
          <form method="POST" action="/profiles/{{$profile->id}}" class="form-group row col-md-12" >
            @csrf
            @method('PUT')
            <div class="col-lg-3 offset-4 mt-5" >
              <label for="FirstName" class="">First Name: </label>
               <input type="text" class="form-control form-control-user" name="firstname" value="{{  Auth::user()->empFirstName }}" readonly>
            </div>

            <div class="col-lg-3  mt-5" >
              <label for="lastname" class="">Last Name: </label>
               <input type="text" class="form-control form-control-user"  name="lastname" value="{{  Auth::user()->empLastName }}" readonly>
            </div>


             <div class="col-lg-6 offset-4 mt-5" >
              <label for="contact" class="">Employee Designation: </label>
               <input type="text" class="form-control form-control-user" id="contact" name="designation"   value="{{ $profile->empDesignation }}" >
               @if($errors->has('designation'))
                   <div class="error" style="color: red;" >{{ $errors->first('designation') }}</div>
               @endif
            </div>

             <div class="col-lg-6 offset-4 mt-5" >
              <label for="city" class="">City: </label>
               <input type="text" class="form-control form-control-user" id="City" name="city"  required="true" value="{{ $profile->city }}">
               @if($errors->has('city'))
                   <div class="error" style="color: red;" >{{ $errors->first('city') }}</div>
               @endif
            </div>


            <div class="col-lg-6 offset-4 mt-5" >
              <label for="contact" class="">Employee Contact: </label>
               <input type="text" class="form-control form-control-user" id="contact" name="contact" aria-describedby="emailHelp"  value="{{ $profile->employeeContact }}">
               @if($errors->has('contact'))
                   <div class="error" style="color: red;" >{{ $errors->first('contact') }}</div>
               @endif
            </div>

            <div class="col-lg-3 offset-4 mt-5">
              <label for="Gender" class="">Employee Gender: </label>
              <select class="form-control form-control-user" id="gender" name="gender">
                <option value="Male" selected>Male</option>
                <option value="Female" >Female</option>
              </select>
            </div>


            <div class="col-lg-3 mt-5">
              <label for="hiredate" class="">Hire Date: </label>
               <input type="date" class="form-control form-control-user" id="hiredate" name="hiredate" aria-describedby="emailHelp"  value="{{ $profile->hire_date }}">
               @if($errors->has('hiredate'))
                   <div class="error" style="color: red;" >{{ $errors->first('hiredate') }}</div>
               @endif
            </div>

            <div class="col-lg-6 offset-4 mt-5">
              <label for="salary" class="">Salary: </label>
               <input type="number" step="0.01" class="form-control form-control-user" id="salary" name="salary" aria-describedby="emailHelp" required="true" value="{{ $profile->salary }}">
               @if($errors->has('salary'))
                   <div class="error" style="color: red;" >{{ $errors->first('salary') }}</div>
               @endif
            </div>
                @if(Auth::user()->id == $profile->user_id)
            <div class="text-center col-lg-6 offset-4 mt-5">
              <button type="submit" class="btn btn-primary btn-lg">Edit Profile</button>
            </div>
                 @endif
           
          </form>
        </div>
     

        </div>
      </main>
    </div>

@endsection