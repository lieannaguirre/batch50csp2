@extends('layouts.app')

@section('content')
<div class="chiller-theme toggled">
@include('sidebar.sidebar')
 <main class="">
    <div class="container-fluid">

        <div class="row">
          <div class="wrapperbody">
            <div class="containerid">
              <img src="{{ asset( $profile->user->img_path) }}" alt="" class="profile-img">
              
              <div class="contentid">
                <div class="sub-content">
                  <h1>{{ $profile->user->empFirstName . ' ' . $profile->user->empLastName }}</h1>
                  <span>{{ $profile->user->email }}</span>
                  @foreach($departments as $department)
                  @if($profile->user->department_code ==  $department->code)
                    <p style="font-size: 22px;">{{ $department->deptName}}</p>
                  @endif
                  @endforeach
                  <p class="location"><i class="fa fa-map-marker" aria-hidden="true"></i>{{ $profile->city }}</p>
                  <p class="location" style="font-size: 24px"><i class="fa fa-id-card-o mr-1" aria-hidden="true" ></i>{{ $profile->employeenumber }}</p>
                </div>

                </div>

              </div>
               @if(Auth::user()->id ==  $profile->user_id)
                  <a href="/profiles/{{ $profile->id }}/edit" class="btn btn-outline-info btn-block btn-lg" style="margin-top: 20px;">Edit Profile</a>  
                  @endif
            </div>
            
          </div>
      
      
            <div class="row">
              <div class="col-lg-3 offset-4 mt-5" >
                <label for="FirstName" class="">First Name: </label>
                 <input type="text" class="form-control form-control-user" id="FirstName" name="lastname" aria-describedby="emailHelp"  value="{{  $profile->user->empFirstName }}" readonly>
              </div>

              <div class="col-lg-3  mt-5" >
                <label for="lastname" class="">Last Name: </label>
                 <input type="text" class="form-control form-control-user" id="lastname" name="lastname"   value="{{  $profile->user->empLastName }}" readonly>
              </div>
             
               <div class="col-lg-6 offset-4 mt-5" >
                <label for="contact" class="">Employee Designation: </label>
                 <input type="text" class="form-control form-control-user" id="designation" name="designation"   value="{{ $profile->empDesignation }}" readonly>
              </div>

              <div class="col-lg-6 offset-4 mt-5">
                <label for="city" class="">City: </label>
                 <input type="text" class="form-control form-control-user" id="City" name="city"  required="true" value="{{ $profile->city }}" readonly>
              </div>


              <div class="col-lg-6 offset-4 mt-5" >
                <label for="contact" class="">Employee Contact: </label>
                 <input type="text" class="form-control form-control-user" id="contact" name="contact" aria-describedby="emailHelp"  value="{{ $profile->employeeContact }}" readonly>
              </div>

              <div class="col-lg-3 offset-4 mt-5">
                <label for="Gender" class="">Employee Gender: </label>
                <select class="form-control form-control-user" id="gender" name="gender" disabled>
                  <option>{{ $profile->employeeGender }}</option>
                  <option value="Male">Male</option>
                  <option value="Female" >Female</option>
                </select>
              </div>


              <div class="col-lg-3 mt-5">
                <label for="hiredate" class="">Hire Date: </label>
                 <input type="date" class="form-control form-control-user" id="hiredae" name="hiredate" aria-describedby="emailHelp"  value="{{ $profile->hire_date }}" readonly>
              </div>

              <div class="col-lg-6 offset-4 mt-5">
                <label for="salary" class="">Salary: </label>
                 <input type="number" step="0.01" class="form-control form-control-user" id="salary" name="salary" aria-describedby="emailHelp" required="true" value="{{ $profile->salary }}" readonly>
              </div>
          </div>
   

  

    </div>
  </main>
</div>
@endsection