
@extends('layouts.app')

@section('content')
<div class=" chiller-theme toggled">
@include('sidebar.sidebar')
	 <main class="">
	    <div class="container-fluid">
	    	@can('auditor')

	        	<div class="jumbotron">
	        	<div class="row w-100" id="boxes">
	        	        <div class="col-md-3" id="boxone">
	        	            <div class="card mx-sm-1 p-3 shadow" id="border">
	        	                <div class="card shadow text-info p-3 my-card " id="border"><p class="fas fa-users m-auto text-primary" aria-hidden="true"><a href="#" class="m-3" >USERS</a></p></div>
	        	                <div class=" text-center mt-3"><h4>REG USERS</h4></div>
	        	                <div class=" text-center mt-2"><h1>{{ count($users) }}</h1></div>
	        	            </div>
	        	        </div>
	        	        <div class="col-md-3" id="boxtwo">
	        	            <div class="card mx-sm-1 p-3 shadow" id="border">
	        	                <div class="card shadow p-3 my-card" id="border"><p class="fas fa-sitemap m-auto text-primary" aria-hidden="true"><a href="#" class="m-3">ASSETS</a></p></div> 
	        	                <div class="text-center mt-3"><h4>LISTED ASSETS</h4></div>
	        	                <div class="text-center mt-2"><h1>{{ count($assets) }}</h1></div>
	        	            </div>
	        	        </div>
	        	        <div class="col-md-3" id="boxthree">
	        	            <div class="card mx-sm-1 p-3 shadow" id="border">
	        	                <div class="card shadow p-3 my-card" id="border"><p class="fas fa-calendar m-auto text-primary" aria-hidden="true"><a href="/transactions" class="m-3">TRANSACTIONS</a></p></div>
	        	                <div class=" text-center mt-3"><h4>TOTAL BOOKINGS</h4></div>
	        	                <div class=" text-center mt-2"><h1>{{ count($transactions) }}</h1></div>
	        	            </div>
	        	        </div>
	        	        <div class="col-md-3" id="boxfour">
	        	            <div class="card mx-sm-1 p-3 shadow" id="border">
	        	                <div class="card shadow p-3 my-card" id="border" ><p class="fab fa-product-hunt m-auto text-primary" aria-hidden="true" style="font-weight: bolder"><a href="/products" class="m-3">PRODUCTS</a></p></div>
	        	                <div class="text-center mt-3"><h4>LISTED PRODUCTS</h4></div>
	        	                <div class="text-center mt-2"><h1>{{ count($products) }}</h1></div>
	        	            </div>
	        	        </div>
	        	     </div>
	        	</div>
	        	@endcan
{{-- HRD VIEW DASHBOARD --}}
	        	@can('hr')

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	      <div class="modal-body">
	        <table class="table table-bordered">
	          <thead>
	            <tr>
	              <th scope="col">#</th>
	              <th scope="col">ID Number</th>
	              <th scope="col">Name</th>
	              <th scope="col">Designation</th>
	              <th scope="col">Department</th>
	              <th scope="col">Hire Date</th>
	            </tr>
	          </thead>
	          <tbody>
	          	@foreach($profiles as $profile)
	            <tr>
	              <th scope="row">{{ $loop->iteration }}</th>
	              <td>{{ $profile->employeenumber }}</td>
	              <td>{{ $profile->user->empFirstName." ". $profile->user->empLastName  }}</td>
	              <td>{{ $profile->empDesignation }}</td>
	              @foreach($departments as $department)
	              @if($profile->user->department_code == $department->code )
	              <td>{{ $department->deptName }}</td>
	              @endif
	              @endforeach
	              <td>{{ $profile->hire_date }}</td>
	            </tr>
				@endforeach	            
	          </tbody>
	        </table>
	    </div>
	  </div>
	</div>
</div>

{{-- end of modal --}}

	        	<div class="jumbotron">
	        	<div class="row w-100" id="boxes">
	        	        <div class="col-md-3" id="boxone">
	        	            <div class="card mx-sm-1 p-3 shadow-lg" id="border">
	        	                <div class="card shadow text-info p-3 my-card " id="border"><p class="fas fa-users m-auto text-primary" aria-hidden="true"><a href="#" class="m-3" data-toggle="modal" data-target=".bd-example-modal-lg">EMPLOYEES</a></p></div>
	        	                <div class=" text-center mt-3"><h4>REG EMPLOYEES</h4></div>
	        	                <div class=" text-center mt-2"><h1>{{ count($users) }}</h1></div>
	        	            </div>
	        	        </div>
	        	        <div class="col-md-3" id="boxthree">
	        	            <div class="card mx-sm-1 p-3 shadow-lg" id="border">
	        	                <div class="card shadow p-3 my-card" id="border"><p class="fas fa-calendar m-auto text-primary" aria-hidden="true"><a href="/transactions" class="m-3">TRANSACTIONS</a></p></div>
	        	                <div class=" text-center mt-3"><h4>TOTAL BOOKINGS</h4></div>
	        	                <div class=" text-center mt-2"><h1>{{ count($transaction) }}</h1></div>
	        	            </div>
	        	        </div>
	        	     </div>
	        	</div>
	        	@endcan

	        	@can('itd')
	        	<div class="jumbotron">
	        	<div class="row w-100" id="boxes">
	        	        <div class="col-md-3" id="boxthree">
	        	            <div class="card mx-sm-1 p-3 shadow-lg" id="border">
	        	                <div class="card shadow p-3 my-card" id="border"><p class="fas fa-calendar m-auto text-primary" aria-hidden="true"><a href="/transactions" class="m-3">TRANSACTIONS</a></p></div>
	        	                <div class=" text-center mt-3"><h4>TOTAL BOOKINGS</h4></div>
	        	                <div class=" text-center mt-2"><h1>{{ count($transaction) }}</h1></div>
	        	            </div>
	        	        </div>
	        	     </div>
	        	</div>
	        	@endcan

	        	@can('opd')
	        	<div class="jumbotron">
	        	<div class="row w-100" id="boxes">
	        	        <div class="col-md-3" id="boxthree">
	        	            <div class="card mx-sm-1 p-3 shadow-lg" id="border">
	        	                <div class="card shadow p-3 my-card" id="border"><p class="fas fa-calendar m-auto text-primary" aria-hidden="true"><a href="/transactions" class="m-3">TRANSACTIONS</a></p></div>
	        	                <div class=" text-center mt-3"><h4>TOTAL BOOKINGS</h4></div>
	        	                <div class=" text-center mt-2"><h1>{{ count($transaction) }}</h1></div>
	        	            </div>
	        	        </div>
	        	     </div>
	        	</div>
	        	@endcan


	    </div>
	</main>
</div>
@endsection
