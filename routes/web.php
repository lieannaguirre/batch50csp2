<?php
use Illuminate\Http\Request; 
use App\Category;
use App\Product;
use App\Asset;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::resource('departments','DepartmentController');
Route::resource('profiles', 'ProfileController');
Route::resource('categories', 'CategoryController');
Route::resource('assets', 'AssetController');
Route::resource('products', 'ProductController');
Route::resource('transactions', 'TransactionController');
Route::get('/dashboard', 'DashboardController@index');

   

