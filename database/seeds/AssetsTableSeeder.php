<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Assets')->delete();
        
        \DB::table('Assets')->insert(array (
            0 => 
            array (
                'id' => 19,
                'serialNo' => 'CPU-1041-97851',
                'isAvailable' => 1,
                'product_id' => 5,
                'created_at' => '2020-04-30 15:18:17',
                'updated_at' => '2020-05-01 11:21:21',
            ),
            1 => 
            array (
                'id' => 20,
                'serialNo' => 'LTP-1015-77731',
                'isAvailable' => 1,
                'product_id' => 7,
                'created_at' => '2020-04-30 15:18:20',
                'updated_at' => '2020-04-30 21:33:49',
            ),
            2 => 
            array (
                'id' => 21,
                'serialNo' => 'LTP-1031-25611',
                'isAvailable' => 1,
                'product_id' => 8,
                'created_at' => '2020-04-30 15:18:23',
                'updated_at' => '2020-04-30 21:34:02',
            ),
            3 => 
            array (
                'id' => 22,
                'serialNo' => 'CPU-1010-88031',
                'isAvailable' => 1,
                'product_id' => 9,
                'created_at' => '2020-04-30 15:18:28',
                'updated_at' => '2020-05-01 11:35:52',
            ),
            4 => 
            array (
                'id' => 23,
                'serialNo' => 'FUR-1042-92701',
                'isAvailable' => 1,
                'product_id' => 13,
                'created_at' => '2020-04-30 15:18:34',
                'updated_at' => '2020-04-30 15:18:34',
            ),
            5 => 
            array (
                'id' => 24,
                'serialNo' => 'FUR-1032-68581',
                'isAvailable' => 1,
                'product_id' => 12,
                'created_at' => '2020-04-30 15:18:38',
                'updated_at' => '2020-04-30 21:34:50',
            ),
            6 => 
            array (
                'id' => 25,
                'serialNo' => 'MON-1001-59861',
                'isAvailable' => 1,
                'product_id' => 11,
                'created_at' => '2020-04-30 15:18:41',
                'updated_at' => '2020-04-30 21:34:33',
            ),
            7 => 
            array (
                'id' => 26,
                'serialNo' => 'MON-1015-90421',
                'isAvailable' => 1,
                'product_id' => 10,
                'created_at' => '2020-04-30 15:18:45',
                'updated_at' => '2020-04-30 21:34:11',
            ),
            8 => 
            array (
                'id' => 27,
                'serialNo' => 'LTP-1033-34141',
                'isAvailable' => 1,
                'product_id' => 7,
                'created_at' => '2020-04-30 15:19:37',
                'updated_at' => '2020-04-30 21:33:49',
            ),
            9 => 
            array (
                'id' => 28,
                'serialNo' => 'LTP-1033-34142',
                'isAvailable' => 1,
                'product_id' => 7,
                'created_at' => '2020-04-30 15:19:37',
                'updated_at' => '2020-04-30 21:33:49',
            ),
            10 => 
            array (
                'id' => 29,
                'serialNo' => 'LTP-1021-86841',
                'isAvailable' => 1,
                'product_id' => 8,
                'created_at' => '2020-04-30 15:19:52',
                'updated_at' => '2020-04-30 21:34:02',
            ),
            11 => 
            array (
                'id' => 30,
                'serialNo' => 'FUR-1024-66320',
                'isAvailable' => 1,
                'product_id' => 12,
                'created_at' => '2020-04-30 15:20:23',
                'updated_at' => '2020-04-30 21:34:50',
            ),
            12 => 
            array (
                'id' => 31,
                'serialNo' => 'MON-1007-10990',
                'isAvailable' => 1,
                'product_id' => 11,
                'created_at' => '2020-04-30 15:20:38',
                'updated_at' => '2020-04-30 21:34:33',
            ),
            13 => 
            array (
                'id' => 32,
                'serialNo' => 'MON-1007-10991',
                'isAvailable' => 1,
                'product_id' => 11,
                'created_at' => '2020-04-30 15:20:38',
                'updated_at' => '2020-04-30 21:34:33',
            ),
            14 => 
            array (
                'id' => 33,
                'serialNo' => 'FUR-1012-77810',
                'isAvailable' => 1,
                'product_id' => 13,
                'created_at' => '2020-04-30 15:21:04',
                'updated_at' => '2020-04-30 15:21:04',
            ),
            15 => 
            array (
                'id' => 34,
                'serialNo' => 'CPU-1044-45750',
                'isAvailable' => 1,
                'product_id' => 5,
                'created_at' => '2020-04-30 21:26:39',
                'updated_at' => '2020-04-30 21:26:39',
            ),
            16 => 
            array (
                'id' => 35,
                'serialNo' => 'CPU-1044-45751',
                'isAvailable' => 1,
                'product_id' => 5,
                'created_at' => '2020-04-30 21:26:39',
                'updated_at' => '2020-04-30 21:26:39',
            ),
            17 => 
            array (
                'id' => 36,
                'serialNo' => 'CPU-1050-74550',
                'isAvailable' => 1,
                'product_id' => 9,
                'created_at' => '2020-04-30 21:27:06',
                'updated_at' => '2020-04-30 21:27:06',
            ),
            18 => 
            array (
                'id' => 37,
                'serialNo' => 'CPU-1050-74551',
                'isAvailable' => 1,
                'product_id' => 9,
                'created_at' => '2020-04-30 21:27:06',
                'updated_at' => '2020-04-30 21:27:06',
            ),
            19 => 
            array (
                'id' => 38,
                'serialNo' => 'MON-1006-58360',
                'isAvailable' => 1,
                'product_id' => 10,
                'created_at' => '2020-04-30 21:27:14',
                'updated_at' => '2020-04-30 21:34:11',
            ),
            20 => 
            array (
                'id' => 39,
                'serialNo' => 'MON-1006-58361',
                'isAvailable' => 1,
                'product_id' => 10,
                'created_at' => '2020-04-30 21:27:14',
                'updated_at' => '2020-04-30 21:34:11',
            ),
            21 => 
            array (
                'id' => 40,
                'serialNo' => 'MON-1006-58362',
                'isAvailable' => 1,
                'product_id' => 10,
                'created_at' => '2020-04-30 21:27:14',
                'updated_at' => '2020-04-30 21:34:11',
            ),
            22 => 
            array (
                'id' => 41,
                'serialNo' => 'MON-1006-58363',
                'isAvailable' => 1,
                'product_id' => 10,
                'created_at' => '2020-04-30 21:27:14',
                'updated_at' => '2020-04-30 21:34:11',
            ),
            23 => 
            array (
                'id' => 42,
                'serialNo' => 'LTP-1042-99260',
                'isAvailable' => 1,
                'product_id' => 8,
                'created_at' => '2020-04-30 21:27:45',
                'updated_at' => '2020-04-30 21:34:02',
            ),
            24 => 
            array (
                'id' => 43,
                'serialNo' => 'LTP-1042-99261',
                'isAvailable' => 1,
                'product_id' => 8,
                'created_at' => '2020-04-30 21:27:45',
                'updated_at' => '2020-04-30 21:34:02',
            ),
        ));
        
        
    }
}