<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Categories')->delete();
        
        \DB::table('Categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Monitor',
                'assetCode' => 'MON',
                'created_at' => '2020-04-22 12:05:24',
                'updated_at' => '2020-04-22 12:05:24',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Furniture',
                'assetCode' => 'FUR',
                'created_at' => '2020-04-22 12:09:33',
                'updated_at' => '2020-04-22 12:09:33',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'C.P.U',
                'assetCode' => 'CPU',
                'created_at' => '2020-04-22 12:10:04',
                'updated_at' => '2020-04-22 12:10:04',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Laptop',
                'assetCode' => 'LTP',
                'created_at' => '2020-04-23 01:10:04',
                'updated_at' => '2020-04-03 01:10:04',
            ),
        ));
        
        
    }
}