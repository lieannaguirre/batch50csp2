<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Statuses')->delete();
        
        \DB::table('Statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Pending',
                'created_at' => '2020-04-21 20:31:49',
                'updated_at' => '2020-04-21 20:28:26',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Canceled',
                'created_at' => '2020-04-21 20:31:49',
                'updated_at' => '2020-04-21 20:28:26',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Rejected',
                'created_at' => '2020-04-22 05:09:52',
                'updated_at' => '2020-04-22 08:23:06',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Approved',
                'created_at' => '2020-04-21 20:31:49',
                'updated_at' => '2020-04-21 20:28:26',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Returned',
                'created_at' => '2020-04-02 05:12:33',
                'updated_at' => '2020-04-22 05:42:31',
            ),
        ));
        
        
    }
}