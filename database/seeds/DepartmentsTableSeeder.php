<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Departments')->delete();
        
        \DB::table('Departments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'deptName' => 'Human Resource Department',
                'code' => 'HRD',
                'created_at' => '2020-04-22 05:42:31',
                'updated_at' => '2020-04-22 05:42:31',
            ),
            1 => 
            array (
                'id' => 2,
                'deptName' => 'Audit Department',
                'code' => 'ADT',
                'created_at' => '2020-04-22 05:42:31',
                'updated_at' => '2020-04-22 05:42:31',
            ),
            2 => 
            array (
                'id' => 3,
                'deptName' => 'Operations Department',
                'code' => 'OPD',
                'created_at' => '2020-04-22 05:42:31',
                'updated_at' => '2020-04-22 05:42:31',
            ),
            3 => 
            array (
                'id' => 4,
                'deptName' => 'IT Department',
                'code' => 'ITD',
                'created_at' => '2020-04-22 05:42:31',
                'updated_at' => '2020-04-22 05:42:31',
            ),
        ));
        
        
    }
}