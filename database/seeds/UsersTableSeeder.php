<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Users')->delete();
        
        \DB::table('Users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'img_path' => 'images/1587843814-profile.jpg',
                'empFirstName' => 'Jane',
                'empLastName' => 'Doe',
                'department_code' => 'HRD',
                'email' => 'hrd@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$ehvsJxCBYFYyW3576QkfEubRGm4yvETiJMMuNzi9qlQQhO9HhfUju',
                'remember_token' => 'Byyeeu0SKP9yYPNANYcXBYHtJBM5h0bJbNxeuFqVedNcVFtM7Uya9XB2FEih',
                'created_at' => '2020-04-25 19:43:34',
                'updated_at' => '2020-04-25 19:43:34',
            ),
            1 => 
            array (
                'id' => 2,
                'img_path' => 'images/1587844994-profile.png',
                'empFirstName' => 'John',
                'empLastName' => 'Doe',
                'department_code' => 'ADT',
                'email' => 'audit@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qOiLPLraK6MxfT/cX.uPGeoLM52NfTnyK.0iuwf16ma8j8807wgHO',
                'remember_token' => NULL,
                'created_at' => '2020-04-25 20:03:14',
                'updated_at' => '2020-04-25 20:03:14',
            ),
            2 => 
            array (
                'id' => 3,
                'img_path' => 'images/1587853284-profile.jpg',
                'empFirstName' => 'Anna',
                'empLastName' => 'Murray',
                'department_code' => 'ITD',
                'email' => 'itd@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$iWfVzI57LIIfkxiDufNnruGJiAuAlMZRH1Q4GOaFHWaOIo6SN8rHa',
                'remember_token' => NULL,
                'created_at' => '2020-04-25 22:21:24',
                'updated_at' => '2020-04-25 22:21:24',
            ),
            3 => 
            array (
                'id' => 4,
                'img_path' => 'images/1587890247-profile.jpg',
                'empFirstName' => 'Sam',
                'empLastName' => 'Murthay',
                'department_code' => 'ITD',
                'email' => 'opd@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$r3mUy25.RkMLlpFd3VNQ5uEaApbx.Ygq1qSq.aGD/NwXvz7kcECOq',
                'remember_token' => NULL,
                'created_at' => '2020-04-26 08:37:27',
                'updated_at' => '2020-04-26 08:37:27',
            ),
        ));
        
        
    }
}