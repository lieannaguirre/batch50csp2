<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Profiles')->delete();
        
        \DB::table('Profiles')->insert(array (
            0 => 
            array (
                'id' => 6,
                'user_id' => 1,
                'employeenumber' => 'HRD-1014-6454',
                'empDesignation' => 'H.R Head',
                'employeeContact' => 9199007866,
                'employeeGender' => 'Male',
                'city' => 'Switzerland',
                'hire_date' => '2020-07-01',
                'salary' => '56000.00',
                'created_at' => '2020-04-25 21:30:10',
                'updated_at' => '2020-04-25 21:32:29',
            ),
            1 => 
            array (
                'id' => 7,
                'user_id' => 2,
                'employeenumber' => 'ADT-1045-1159',
                'empDesignation' => 'Assistant Auditor',
                'employeeContact' => 9199007866,
                'employeeGender' => 'Male',
                'city' => 'Singapore',
                'hire_date' => '2010-04-03',
                'salary' => '78000.00',
                'created_at' => '2020-04-25 21:34:24',
                'updated_at' => '2020-04-25 21:34:24',
            ),
            2 => 
            array (
                'id' => 8,
                'user_id' => 3,
                'employeenumber' => 'ITD-1014-8813',
                'empDesignation' => 'I.T Head',
                'employeeContact' => 9876657898,
                'employeeGender' => 'Female',
                'city' => 'Canada',
                'hire_date' => '2012-09-04',
                'salary' => '99000.00',
                'created_at' => '2020-04-25 22:22:13',
                'updated_at' => '2020-04-25 22:22:13',
            ),
            3 => 
            array (
                'id' => 9,
                'user_id' => 4,
                'employeenumber' => 'ITD-1009-3879',
                'empDesignation' => 'O.P Head',
                'employeeContact' => 9199007988,
                'employeeGender' => 'Male',
                'city' => 'Mumbai',
                'hire_date' => '2019-08-06',
                'salary' => '88000.00',
                'created_at' => '2020-04-26 09:37:06',
                'updated_at' => '2020-04-26 10:09:32',
            ),
        ));
        
        
    }
}