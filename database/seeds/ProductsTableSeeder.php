<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Products')->delete();
        
        \DB::table('Products')->insert(array (
            0 => 
            array (
                'id' => 5,
                'isActive' => 1,
                'name' => 'Inspiron 3671 Small Desktop',
                'description' => 'Improved design, packed with impressive power Featuring an updated, contemporary design, the Inspiron desktop has been reimagined to enhance the enjoyment that comes with owning a Dell desktop.  Small and mighty:',
                'img_path' => 'images/1588248459.jpg',
                'category_id' => 3,
                'created_at' => '2020-04-30 11:51:06',
                'updated_at' => '2020-04-30 13:22:30',
            ),
            1 => 
            array (
                'id' => 7,
                'isActive' => 1,
                'name' => 'Inspiron 14 5000 Laptop',
                'description' => 'Fresh design Refined colors: Choose from a variety of contemporary and calming colors, like Platinum Silver, Iced Lilac, Iced Gold and Iced Mint.  Sleek interior: The newly redesigned power button is neatly incorporated into the keyboard for a clean and modern look.',
                'img_path' => 'images/1588248721.jpg',
                'category_id' => 4,
                'created_at' => '2020-04-30 12:12:01',
                'updated_at' => '2020-04-30 13:02:20',
            ),
            2 => 
            array (
                'id' => 8,
                'isActive' => 1,
                'name' => 'Inspiron 15 7590 Series',
                'description' => 'Lightweight with surprising performance 15.6-inch laptop constructed of thin and light Magnesium-Alloy, 9th Gen Intel® Core™ H-Class processors and high-powered NVIDIA® GTX graphics.',
                'img_path' => 'images/1588248844.jpg',
                'category_id' => 4,
                'created_at' => '2020-04-30 12:14:04',
                'updated_at' => '2020-04-30 12:14:04',
            ),
            3 => 
            array (
                'id' => 9,
                'isActive' => 1,
                'name' => 'Dell G5 Gaming Desktop',
                'description' => 'Game changer. Space saver. Powerful, compact gaming desktop with easy upgrade ability and up to 9th Gen Intel® Core™ i9, VR-capable GPUs and up to 32GB DDR4 RAM.',
                'img_path' => 'images/1588248956.jpg',
                'category_id' => 3,
                'created_at' => '2020-04-30 12:15:56',
                'updated_at' => '2020-04-30 12:15:56',
            ),
            4 => 
            array (
                'id' => 10,
                'isActive' => 1,
                'name' => 'Inspiron 24 5000',
                'description' => 'Design worth displaying Brilliantly designed 24 inch all in one desktop with the latest 10th Gen Intel® Core™ processors, InfinityEdge display, space efficient stand, Dell Cinema and a pop up webcam.',
                'img_path' => 'images/1588249188.jpg',
                'category_id' => 1,
                'created_at' => '2020-04-30 12:19:48',
                'updated_at' => '2020-04-30 12:19:48',
            ),
            5 => 
            array (
                'id' => 11,
                'isActive' => 1,
                'name' => 'Inspiron 22 3000',
                'description' => 'Entertainment made effortless 22-inch all-in-one desktop with advanced streaming ideal for watching video or chatting. Featuring Full HD, wide-angle IPS display with pop-up webcam.',
                'img_path' => 'images/1588249246.jpg',
                'category_id' => 1,
                'created_at' => '2020-04-30 12:20:46',
                'updated_at' => '2020-04-30 13:24:10',
            ),
            6 => 
            array (
                'id' => 12,
                'isActive' => 1,
                'name' => 'Clifford High Back Office Chair',
                'description' => 'Back : Metal Tube Frame Covered With Open Mesh Fabric &amp; Polyester Fabric On Side Seat: Plywood With Foam On Seat, Covered With Polyester Fabric Gas Lift: 10cm Length Black Gas Lift',
                'img_path' => 'images/1588249431.jpg',
                'category_id' => 2,
                'created_at' => '2020-04-30 12:23:51',
                'updated_at' => '2020-04-30 12:23:51',
            ),
            7 => 
            array (
                'id' => 13,
                'isActive' => 1,
                'name' => 'Michel High Back Office Chair',
                'description' => '•  Head Rest: Nylon Plastic Covered With Mesh Fabric 
• Back Rest: Nylon Plastic Frame Covered With Mesh Fabric
• Seat: Plywood Frame With Recycle Foam Covered With Polyester Fabric',
                'img_path' => 'images/1588249578.jpg',
                'category_id' => 2,
                'created_at' => '2020-04-30 12:26:18',
                'updated_at' => '2020-04-30 13:23:07',
            ),
        ));
        
        
    }
}