<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');  // FOREIGN
            $table->string('employeenumber')->unique()->nullable();  //to be edited by HR
            $table->string('empDesignation',55)->nullable(); //to be edited by HR
            $table->bigInteger('employeeContact')->nullable(); 
            $table->enum('employeeGender', ['Male', 'Female'])->nullable();
            $table->string('city')->nullable();
            $table->date('hire_date')->nullable();//can be edited by HR
            $table->decimal('salary', 10, 2)->nullable();  //to be edited by HR
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('restrict')
            ->onUpdate('cascade');

          

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
