<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Profile;
use App\Department;
use App\User;
use App\Product;
use App\Category;
use Auth;
use App\Asset;
use Session; 

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
      if(Auth::user())
      {
        $transactions = Transaction::orderBy('created_at','desc')->get();
        return view('transactions.index',compact('transactions'));
       }else{
        abort(404);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $request->validate([
            'borrowed' => 'required|date|after:today',
            'returned' => 'required|date|after_or_equal:borrowed',
          
        ]);

        $borrowed = htmlspecialchars($request->input('borrowed'));
        $returned = htmlspecialchars($request->input('returned'));
        $prodid = htmlspecialchars($request->input('prodid')); 
        $serial = htmlspecialchars($request->input('prodserial'));
        $products = Product::find($prodid);
        $cat = $products->category_id;
        $catname = Category::find($cat);
        $categoryname = $catname->assetCode;
        $empCode = Auth::user()->department_code;


       // alphanumeric Code
        do{
            $codeone = mt_rand(101,999);
            $codetwo = mt_rand(10001,99999);
            $rand = $categoryname.$codeone.$empCode.$codetwo;
        }while(!empty( Transaction::where('referenceNo',$rand)->first()));
        

            $transaction = new Transaction;
            $transaction->referenceNo = $rand;
            $transaction->user_id = Auth::user()->id;
            $transaction->product_id = $prodid;
            $transaction->borrowDate = $borrowed;
            $transaction->returnedDate=$returned;
            
            

                $transaction->save();
                //this will now call the index action of the productcontroller as per laravels resourceful routes
                return redirect('/transactions');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {

        $this->authorize('create', Product::class);

        $approved = htmlspecialchars($request->input('approved'));
        $rejected = htmlspecialchars($request->input('rejected'));
        $returned = htmlspecialchars($request->input('returned'));
        $asset = Asset::where('product_id',$transaction->product_id)->first();
      


       
               do{
            $codeone = mt_rand(101,999);
            $codetwo = mt_rand(10001,99999);
            $rand = $codeone.$codetwo;
        }while(!empty( Transaction::where('referenceNo',$rand)->first()));

        if($approved == 'Approved')
        {
            $transaction->status_id = 4;
           $transaction->asset_serial = $asset->serialNo;
           $asset->isAvailable=0;
           $asset->save();
           

        }elseif($rejected == 'Rejected'){
            $transaction->status_id = 3;

            
        }else{
            $transaction->status_id = 5;
            $asset->isAvailable = 1;
            $asset->save();
        }

        $transaction->save();
        return redirect('/transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
       
        if(Auth::user())
        {
            if($transaction->status_id == 1)
            {
                $transaction->status_id = 2;
                $transaction->save();

            }
            
            return redirect('/transactions');
        }
    }
}
