<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Category;
use App\Transaction;
use App\User;
use App\Asset;
use App\Profile;
use App\Department;

class DashboardController extends Controller
{
   public function index()
    {

    	$categories = Category::all();
    	$transactions = Transaction::all();
    	$products = Product::all();
    	$users = User::all();
    	$assets = Asset::all();
    	$profiles = Profile::all();
    	$departments = Department::all();
    	$transaction = Transaction::where('user_id', Auth::user()->id)->get();

    	return view('dashboard.index',compact('categories','transactions','products','users','assets', 'transaction','profiles','departments'));
    }
}
