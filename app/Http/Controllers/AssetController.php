<?php

namespace App\Http\Controllers;

use App\Asset;
use Illuminate\Http\Request;
use App\Product;
use App\Category;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return abort(404);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {   
        $this->authorize('create', Asset::class);

        $quantity = htmlspecialchars($request->input('quantity'));
        $prodid = htmlspecialchars($request->input('prodid')); 
    
        $product = Product::find($prodid); // product id
        $cat = $product->category_id; // product id to category
        $catname = Category::find($cat); //access the category 
        $categoryname = $catname->assetCode;
        
        //system generated code for serial Number
        do{
            $codeone = mt_rand(1001,1056);
            $codetwo = mt_rand(1001,9999);
            $rand = $categoryname."-".$codeone."-".$codetwo;

        }while(!empty( Asset::where('serialNo',$rand)->first()));

            //for loop to generate assets depending on the quantity input

            for ($i = 0; $i< $quantity; $i++)
               {
                $asset = new Asset;

                $asset->serialNo = $rand.$i;
                $asset->product_id = $prodid;
                $asset->save();
               }
            return redirect('/products');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        //
    }
}
