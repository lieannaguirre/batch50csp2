<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use App\Department;
use Illuminate\Http\Request;
use Auth;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::where('user_id', Auth::user()->id)->get();
        $departments = Department::all();
        if($profiles)
        {
            foreach($profiles as $profile)
            if(isset($profile->id))
            {
                return redirect ('/profiles/'.$profile->id);
            }else{
                
            }
            return redirect('/profiles/create');
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if(auth::user())
        {
         $departments = Department::all();
         return view('profiles.create',compact('departments'));
        }
        
   
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Profile $profile)
    {
      
             $request->validate([
                'designation' => 'string',
                'city' => 'string',
                'contact' => 'numeric',
                'gender' => 'string',
                'hiredate' => 'date'


            ]);

             do{
                $codeone = mt_rand(1001,1056);
                $codetwo = mt_rand(1001,9999);
                $rand = Auth::user()->department_code."-".$codeone."-".$codetwo;
              }while(!empty( Profile::where('employeenumber',$rand)->first()));

                $designation = htmlspecialchars($request->input('designation'));
                $city = htmlspecialchars($request->input('city'));
                $contact = htmlspecialchars($request->input('contact'));
                $gender = $request->input('gender');
                $hiredate = $request->input('hiredate');
                $salary = htmlspecialchars($request->input('salary'));
                
                $profile = new Profile;

                $profile->employeenumber = $rand;
                $profile->user_id = Auth::user()->id;
                $profile->empDesignation = $designation;
                $profile->employeeContact = $contact;
                $profile->employeeGender = $gender;
                $profile->city = $city;
                $profile->hire_date = $hiredate;
                $profile->salary = $salary;

                $profile->save();
            
                return redirect('/profiles/'.$profile->id); 
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        
        $departments = Department::all();
        $profiles = Profile::all();
        $user = User::all();
     
        return view('profiles.show',compact('departments', 'profile','user'));

      
    }

    /**
     * Show th>ie specified resource.
     *
     * @param >ile
     * @return>iponse
     */
    public function edit(Profile $profile)
    {

         $departments = Department::all();

          if(Auth::user()->id == $profile->user_id)
          {
            return view('profiles.edit',compact('departments', 'profile'));
        }else{
            return redirect('/profiles/'.$profile->id); 
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {

        if(Auth::user()->id == $profile->user_id)
        {
          $request->validate([
            'designation' => 'string',
            'city' => 'string',
            'contact' => 'numeric',
            'gender' => 'string',
            'hiredate' => 'date',


        ]);

            $designation = htmlspecialchars($request->input('designation'));
            $city = htmlspecialchars($request->input('city'));
            $contact = htmlspecialchars($request->input('contact'));
            $gender = $request->input('gender');
            $hiredate = $request->input('hiredate');
            $salary = htmlspecialchars($request->input('salary'));
            

            $profile->empDesignation = $designation;
            $profile->employeeContact = $contact;
            $profile->employeeGender = $gender;
            $profile->city = $city;
            $profile->hire_date = $hiredate;
            $profile->salary = $salary;

            $profile->save();
        }
            return redirect("/profiles/".$profile->id); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
