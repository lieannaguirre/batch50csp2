<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->authorize('create', Category::class);

        $request->validate([
            'category' => 'required|string|unique:categories,name',
            'assetcode' => 'required|string|unique:categories,assetCode',
    
        ]);

        $category = htmlspecialchars($request->input('category'));
        $assetcode = htmlspecialchars($request->input('assetcode'));
       
        if(isset($category)){
            //check if data type is correct and is not empty
            if((gettype($category) === "string" && $category != "")){
              
                    //sanitize our input
                    $cleanName = htmlspecialchars($category);
                    $catCode = htmlspecialchars($assetcode);
                    //instantiate a new Category object from the Category model
                    $category = new Category;
                    //set the value of this category's name to be the sanitized form input
                    $category->name = $cleanName;
                    $category->assetCode = $catCode;
                    $category->save();
            }
            return redirect('/products/create'); 
    
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
