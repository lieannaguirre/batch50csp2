<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;
use Auth;
use App\Asset;
use App\Transaction;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user())
        {
            $products= Product::all();
            $assets=Asset::all();
            $transactions=Transaction::all();
          
            return view('products.index',compact('products', 'assets', 'transactions'));
        }else{
            abort(404);
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Product::class);

        $categories = Category::all();
        return view('products.create')-> with ('categories', $categories);    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Product::class);
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'category' =>'required',
            'image' => 'required|image'
        ]);

        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $image = $request->file('image');
        $category = htmlspecialchars($request->input('category'));
        

        $product = new Product;
        $product->name = $name;
        $product->description= $description;
        $product->category_id = $category;

        //handle the file image upload
        if($request->file('image') != null)
        {
            $file_name = time() . "." . $image->getClientOriginalExtension();
            //set target destination where the file will be saved id
            $destination = "images/";
            //call the move() method of the $image object to save the uploaded file in the target destination under the speciied file name
            $image->move($destination, $file_name);
            //set the path of the saved image as the value for the column img_path of this record

            $product->img_path = $destination.$file_name;
        //save the new product object as a new record in the products table via its save() method
        }
            $product->save();

            return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        if(Auth::user())
        {

        $categories = Category::where('id', $product->category->id)->get();
        return view('products.show',compact('categories','product'));
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('update', Product::class);
        $categories = Category::where('id', $product->category->id)->get();
        return view('products.edit',compact('categories','product'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('update', Product::class);

        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'category' =>'required',
            'image' => 'image'
        ]);

        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $image = $request->file('image');
        $category = htmlspecialchars($request->input('category'));

        $product->name = $name;
        $product->description = $description;

        $product->category_id = $category;

        if($request->file('image') != null)
            {
                $file_name = time() . "." . $image->getClientOriginalExtension();
                $destination = "images/";
                $image->move($destination, $file_name);

                $product->img_path = $destination.$file_name;
            }
                $product->save();
                return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $assets = Asset::where('product_id',$product->id)->get();
        $transactions = Transaction::where('product_id', $product->id)->where('status_id', 4)->get();

        
         if($product->isActive == 1){
            $product->isActive = 0;
            foreach($assets as $asset)
            {
                $asset->isAvailable = 0;
                $asset->save();
            }
        }else{
            $product->isActive = 1;
            foreach($assets as $asset)
            {
                if($transactions->count()>0)
                {
                    foreach($transactions as $transaction)
                    {
                        if($transaction->asset_serial != $asset->serialNo)
                        {
                            $asset->isAvailable = 1;
                            $asset->save();
                        }
                    }
                }else{
                    $asset->isAvailable = 1;
                        $asset->save();
                }
               
            }
        }
            $product->save();

        return redirect('/products');
    }
    
}
