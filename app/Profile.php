<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $table = 'profiles';
    protected $fillable = [
         'user_id','employeenumber','empDesignation','employeeContact','employeeGender','city', 'hire_date','salary'
        ];
    public function user()
    {
    	return $this->belongsTo('\App\User');
    }


}
