<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\profile;
use Auth;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('auditor', function($user){
            return $user->department_code === 'ADT';
        });
        Gate::define('hr', function($user){
            return $user->department_code === 'HRD';
        });
         Gate::define('itd', function($user){
            return $user->department_code === 'ITD';
        });
          Gate::define('opd', function($user){
            return $user->department_code === 'OPD';
        });
      
        //
    }
}
