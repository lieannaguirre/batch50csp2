<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'assets';
    
	protected $fillable = [
         'serialNo','isAvailable','product_id'
    ];
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
