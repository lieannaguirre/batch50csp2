<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
	protected $fillable = [
         'name','description','img_path','category_id',
     ];
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function assets()
    {
        return $this->hasMany('App\Asset');
    }
     public function transactions()
    {
        return $this->hasMany('App\Product');
    }

}
