<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
         'user_id','asset_id','product_id',
     ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function asset()
    {
        return $this->belongsTo('App\Asset');
    }
    public function product()
    {
        return $this->belongsTo('App\Product');
    }


}
