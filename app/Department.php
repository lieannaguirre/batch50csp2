<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	protected $table = 'departments';
	protected $fillable = [
         'deptName','code',
     ];
    public function users()
    {
        return $this->hasMany('App\User');
    }
   
}

